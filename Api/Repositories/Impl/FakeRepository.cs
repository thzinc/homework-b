﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Repositories.Impl
{
    public class FakeRepository : IRepository
    {
        private static readonly Random Random = new Random();
        private static readonly IList<Stop> Stops = Enumerable.Range(1, 10)
            .Select((stopNumber, i) => new Stop
            {
                Id = Random.Next(1000 + (i * 500), 1500 + (i * 500)),
                Name = stopNumber.ToString(),
            })
            .ToList();

        private static readonly IList<Route> Routes = Enumerable.Range(1, 3)
            .Select((routeNumber, i) => new Route
            {
                Name = routeNumber.ToString(),
                StartTime = new TimeSpan(0, 0, i * 2, 0),
                Period = 15,
            })
            .ToList();

        private static readonly IList<RouteStop> RouteStops;

        static FakeRepository()
        {
            RouteStops = Routes.SelectMany(route => Stops
                .Select((stop, i) => new RouteStop
                {
                    Route = route,
                    Stop = stop,
                    Increment = new TimeSpan(0, i * 2, 0),
                }))
                .ToList();
        }

        public IEnumerable<T> GetAll<T>()
            where T : BaseEntity
        {
            if (typeof(T) == typeof(Stop))
                return Stops.Cast<T>();

            if (typeof(T) == typeof(Route))
                return Routes.Cast<T>();

            if (typeof(T) == typeof(RouteStop))
                return RouteStops.Cast<T>();

            throw new NotImplementedException();
        }

        public T Get<T>(int id)
            where T : BaseEntity
        {
            return GetAll<T>().Single(x => x.Id == id);
        }
    }
}
