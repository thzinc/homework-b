﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Repositories
{
    public interface IRepository
    {
        IEnumerable<T> GetAll<T>() where T : BaseEntity;

        T Get<T>(int id) where T : BaseEntity;
    }
}
