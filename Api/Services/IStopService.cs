﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.Models.Transformations;

namespace Api.Services
{
    public interface IStopService
    {
        IEnumerable<Stop> Search(string search);
        IEnumerable<Arrival> GetArrivals(DateTime now, int stopId);
    }
}
