﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Api.Models;
using Api.Models.Transformations;
using Api.Repositories;

namespace Api.Services.Impl
{
    public class StopService : IStopService
    {
        private readonly IRepository _repostiory;

        public StopService(IRepository repostiory)
        {
            _repostiory = repostiory;
        }

        public IEnumerable<Stop> Search(string search)
        {
            var pattern = new Regex(Regex.Escape(search ?? ""), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

            return _repostiory.GetAll<Stop>()
                .Where(s => pattern.IsMatch(s.Name));
        }

        public IEnumerable<Arrival> GetArrivals(DateTime now, int stopId)
        {
            return _repostiory.GetAll<RouteStop>()
                .Where(rs => rs.Stop.Id == stopId)
                .Select(rs => new Arrival
                {
                    RouteName = rs.Route.Name,
                    UpcomingArrivals = Enumerable.Range(1, 2)
                        .Select(futurePeriods => CalculateUpcomingArrivalDate(now, rs.Route.StartTime, rs.Increment, rs.Route.Period, futurePeriods))
                        .ToList(),
                });
        }

        private DateTime CalculateUpcomingArrivalDate(DateTime now, TimeSpan startTime, TimeSpan stopIncrement, int period, int futurePeriods)
        {
            var periods = (int) Math.Floor((now.TimeOfDay - startTime).TotalMinutes/period) + futurePeriods;

            return now.Date + startTime + stopIncrement + new TimeSpan(0, periods * period, 0);
        }
    }
}