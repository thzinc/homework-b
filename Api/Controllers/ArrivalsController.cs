﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Api.Models;
using Api.Models.Transformations;
using Api.Services;

namespace Api.Controllers
{
    [RoutePrefix("api/1")]
    public class ArrivalsController : ApiController
    {
        private readonly IStopService _stopService;

        public ArrivalsController(IStopService stopService)
        {
            _stopService = stopService;
        }

        [Route("stops")]
        public IEnumerable<Stop> GetStops(string search)
        {
            return _stopService.Search(search);
        }

        [Route("stop/{stopId:int}/arrivals")]
        public IEnumerable<Arrival> GetArrivalsForStop(int stopId)
        {
            return _stopService.GetArrivals(DateTime.Now, stopId);
        } 
    }
}