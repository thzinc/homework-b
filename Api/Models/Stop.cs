﻿namespace Api.Models
{
    public class Stop : BaseEntity
    {
        public string Name { get; set; }
    }
}