﻿using System;

namespace Api.Models
{
    public class RouteStop : BaseEntity
    {
        public Route Route { get; set; }
        public Stop Stop { get; set; }
        public TimeSpan Increment { get; set; }
    }
}