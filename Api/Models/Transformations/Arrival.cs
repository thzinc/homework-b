﻿using System;
using System.Collections.Generic;

namespace Api.Models.Transformations
{
    public class Arrival
    {
        public string RouteName { get; set; }
        public IList<DateTime> UpcomingArrivals { get; set; }
    }
}