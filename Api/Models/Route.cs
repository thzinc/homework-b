﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class Route : BaseEntity
    {
        /// <summary>
        /// Name of the route
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Time of day when this route begins servicing its first stop
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// Period, in minutes, that this route services each stop
        /// </summary>
        public int Period { get; set; }
    }
}