﻿using System;

namespace Api.Models
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}