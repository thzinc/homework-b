Daniel James' Implementation of "Homework-B" 
============================================

Entity Definitions
------------------

Given the description of the data, I think a focused implementation largely revolves around three entities: a Route, a Stop, and a relationship with some additional data between the two.

![Entity Diagram showing relationship of Route, Stop, and RouteStop entities, with annotations](entity_diagram.png)

A Route is essentially comprised of a name (e.g. "90", "Red Line", "Expo Line"), a start time, and a period indicating the frequency by which the route services the stop. Because the homework describes all routes as running 24 hours per day, end time is not included in my focused implementation, but I believe the real-world use case would likely include defining an end time.

A Stop is, in this focused implementation, simply a name. (e.g. "8690", "Hill St.", "Pershing Square") It would not be difficult to extend the entity to include geolocation information so that an implementation might expose a "stops near you" feature.

A RouteStop is simply a relationship between a Route and a Stop and an increment of time from the Route's start time that the stop will be serviced.


API Definitions
---------------

I have chosen to implement a RESTful API using WebAPI because it's simple, decently powerful, and largely does what it needs to without much fuss. 

I have chosen to prefix the API URLs with /api/1 to 1) allow the API to share the same domain as the service that will serve up the markup and 2) to include a version number so that subsequent breaking changes may be done without breaking older clients.

### GET /api/1/stops?search=term

This takes in a `search` querystring parameter that searches the name of the Stop. It returns an array of Stop objects, each of which has a Name and an Id.

Example response for `search` "`1`":

~~~~
[
  {
    "Id": 1188,
    "Name": "1"
  },
  {
    "Id": 5948,
    "Name": "10"
  }
]
~~~~

### GET /api/1/stop/:stopId/arrivals

This takes in a `stopId` in the URI. It returns an array of Arrival objects for the specified Stop, which are comprised of a RouteName and a list of future dates at which the Stop is expected to be serviced for the Route.

Example response for `stopId` "`1188`" at 08:03 PM PDT:

~~~~
[
  {
    "RouteName": "1",
    "UpcomingArrivals": [
      "2016-09-26T20:15:00-07:00",
      "2016-09-26T20:30:00-07:00"
    ]
  },
  {
    "RouteName": "2",
    "UpcomingArrivals": [
      "2016-09-26T20:17:00-07:00",
      "2016-09-26T20:32:00-07:00"
    ]
  },
  {
    "RouteName": "3",
    "UpcomingArrivals": [
      "2016-09-26T20:19:00-07:00",
      "2016-09-26T20:34:00-07:00"
    ]
  }
]
~~~~


User Interface Definitions
--------------------------

I have chosen to implement the user interface using ReactJS for building out individual components. The flow describes a very basic, two-screen interface that allows a user to search for a Stop and view Routes with upcoming arrivals for that Stop.

![Flowchart diagram showing the relationship between user, client, and server](user_experience_flow.png)

### App

This is the root-level component that handles UI routing.

#### Search

This is the component that encompasses the "Search" view, which is made up of a text input and a list of results. This is the first thing a user sees upon loading the site.

##### Search Box

This is a simple text input. There is no "Search" button, so the search should be triggered once the user stops typing. (Debounce with a 500ms delay.)

##### Stops Container

This is the display of a list of Stops. It should display a Stop component for each result with 1rem of margin between results.

##### Stop

This is the display of an individual Stop. It should simply display the name of the stop as a link.

#### Display

This is the component that encompasses the "Display" view, which is made up of a heading that displays the Stop name and a list of Routes for the stop, each with two upcoming arrival times.

##### Header

This is a that should display the currently-selected Stop name. It should also include a button to go back to the Search view. There is no requirement to persist the search results that may have led to this view.

##### Routes Container

This is the display of a list of Routes. It should display a Route component for each result with 1rem of margin between the results.

##### Route

This is the display of an individual Route. It should display the Route name and two instances of the Arrival Time component.

##### Arrival Time

This is the display of the arrival time for a route. It should take in a date and display the integral number of minutes remaining (rounded up so that "1.5" minutes should read "2 mins"). If there is a singular minute to be displayed, it should read "1 min". Otherwise, it should read "X mins" where X is not 1.