import React from 'react'
import ReactDOM from 'react-dom'
import { browserHistory, Router, Route, IndexRoute } from 'react-router'
import App from './components/App'
import Display from './components/Display'
import Search from './components/Search'

ReactDOM.render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Search}/>
            <Route path="stop/:stopId" component={Display}/>
        </Route>
    </Router>
), document.getElementById('app'));