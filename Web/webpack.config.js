var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './index.js',
  output: { path: '../Api/Scripts', filename: 'bundle.js' },
  module: {
    loaders: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },
};