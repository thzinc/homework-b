import React from 'react'
import autoBind from 'react-autobind'
import SearchBox from './SearchBox'
import StopsContainer from './StopsContainer'

export default class Search extends React.Component {
	constructor() {
		super();
		this.state = {
			stops: [],
		};
		autoBind(this);
	}

	onResults(stops) {
		this.setState({
			stops: stops
		});
	}

	render() {
		return (
			<div>
				<SearchBox onResults={this.onResults}/>
				<StopsContainer stops={this.state.stops}/>
			</div>
		);
	}
}