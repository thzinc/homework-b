import React from 'react'
import debounce from 'debounce'
import autoBind from 'react-autobind'

export default class SearchBox extends React.Component {
	constructor() {
		super();
		this.state = {
			search: ''
		};
		autoBind(this);
	}

	handleChange(evt) {
		this.setState({
			search: evt.target.value
		})
	}

	onKeyUp() {
		// TODO: Call service that calls api/1/stops?search=
		this.props.onResults([{
			Id: 1010,
			Name: "10"
		}]);
	}

	render() {
		var debouncedKeyUp = debounce(this.onKeyUp, 500);
		return (
			<div>
				<input type="search" placeholder="Search for a stop" value={this.state.search} onChange={this.handleChange} onKeyUp={debouncedKeyUp}/>
			</div>
		);
	}
}