import React from 'react'
import { Link } from 'react-router'

export default class Stop extends React.Component {
	render() {
		return (
			<li>
				<Link to={`/stop/${this.props.stop.Id}`}>{this.props.stop.Name}</Link>
			</li>
		);
	}
}