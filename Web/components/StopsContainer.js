import React from 'react'
import Stop from './Stop'

export default class StopsContainer extends React.Component {
	render() {
		return (
			<ul>
				{this.props.stops.map(stop => <Stop key={stop.Id} stop={stop}/>)}
			</ul>
		);
	}
}